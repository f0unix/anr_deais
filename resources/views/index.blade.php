<!DOCTYPE HTML>
<html lang="{{ config('app.locale') }}">
	<head>
		<title>ANR DEAIS</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<header id="header" class="container">
						<div class="row">
							<div class="12u">

								<!-- Logo -->
									<div id="logo_title_header">
										<a href="#">
											<!--<div id="logo_header"><img class="logo_header_img" height="90" width="90" src="{{ asset('images/logo.png') }}" /></div>-->
											<div class="6u" id="logo_title">ANR DéAIS Project</div>
										</a>
									</div>

								<!-- Nav -->
									<nav id="nav">
										<a href="/">Home</a>
										<a href="#features-wrapper">Description</a>
										<a href="#content-wrapper">Information</a>
										<a href="#footer-wrapper">Contact us</a>
									</nav>

							</div>
						</div>
					</header>
					<div id="banner">
						<div class="container">
							<div class="row">
								<div class="6u 12u(mobile)">

									<!-- Banner Copy -->
										<p>
										{{ $sections[0]->section_description }}
										</p>
										

								</div>
								<div class="6u 12u(mobile)">

									<!-- Banner Image -->
										<div class="bordered-feature-image header_image_first"><img src="{{$image_url}}{{$sections[0]->section_image}}" alt="" /></div>

								</div>
							</div>
						</div>
					</div>
				</div>

			<!-- Features -->
				<div id="features-wrapper" name="resume">
					<div id="features">
						<div class="container">
							<div class="row">
								<div class="3u 12u(mobile)">

									<!-- Feature #1 -->
										<section>
											<a href="#" class="bordered-feature-image"><img src="{{$image_url}}{{$sections[1]->section_image}}" alt="" /></a>
											<h2>{!!$sections[1]->section_nom!!}</h2>
											{!!$sections[1]->section_description!!}
										</section>

								</div>
								<div class="3u 12u(mobile)">
									<!-- Feature #2 -->
										<section>
											<a href="#" class="bordered-feature-image"><img src="{{$image_url}}{{$sections[2]->section_image}}" alt="" /></a>
											<h2>{!!$sections[2]->section_nom!!}</h2>
											{!!$sections[2]->section_description!!}
										</section>
								</div>
								<div class="3u 12u(mobile)">
									<!-- Feature #3 -->
										<section>
											<a href="#" class="bordered-feature-image"><img src="{{$image_url}}{{$sections[3]->section_image}}" alt="" /></a>
											<h2>{!!$sections[3]->section_nom!!}</h2>
											{!!$sections[3]->section_description!!}
										</section>
								</div>
								<div class="3u 12u(mobile)">
									<!-- Feature #4 -->
										<section>
											<a href="#" class="bordered-feature-image"><img src="{{$image_url}}{{$sections[4]->section_image}}" alt="" /></a>
											<h2>{!!$sections[4]->section_nom!!}</h2>
											{!!$sections[4]->section_description!!}
										</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- Content -->
				<div id="content-wrapper">
					<div id="content">
						<div class="container">
							<div class="row">
								<div class="4u 12u(mobile)">
									<!-- Box #1 -->
										<section>
											<header>
												<h2>Consortium</h2>
											</header>
											<ul class="quote-list">
												<li>
														<div><img src="{{$image_url}}{{$sections[6]->section_image}}" alt="" /></div>
														<span>{!! $sections[6]->section_description !!}</span>
												</li>
												<li>
														<div><img src="{{$image_url}}{{$sections[7]->section_image}}" alt="" /></div>
														<span>{!! $sections[7]->section_description !!}</span>
												</li>
												<li>
														<div><img src="{{$image_url}}{{$sections[8]->section_image}}" alt="" /></div>
														<span>{!! $sections[8]->section_description !!}</span>
												</li>
												<li>
														<div><img src="{{$image_url}}{{$sections[9]->section_image}}" alt="" /></div>
														<span>{!! $sections[9]->section_description !!}</span>
												</li>							
											</ul>
										</section>

								</div>
								<div class="4u 12u(mobile)">

									<!-- Box #2 -->
										<section>
											<header>
												<h2>Publications</h2>
											</header>
											<ul class="check-list">
												@foreach ($publications as $publication)
						    						<li><a href="{{$publication->publication_lien}}"> {!! $publication->publication_description !!} </a></li>
												@endforeach									
											</ul>
										</section>

								</div>
								<div class="4u 12u(mobile)">

									<!-- Box #3 -->
										<section>
											<header>
												<h2>Funding</h2>
											</header>
											<ul class="quote-list">
												@foreach ($partners as $partner)
												<li>
													<a href="{{$partner->partenaire_lien}}">
														<div><img src="{{$image_url}}{{$partner->partenaire_logo}}" alt="" /></div>
														<p>{{ $partner->partenaire_nom }}</p>
														<span>{{ $partner->partenaire_description }}</span>
													</a>
												</li>
												@endforeach
											</ul>
											<div id="separator"></div>
											<header>
												<h2>Labeling</h2>
											</header>
											<ul class="quote-list">
												<li>
													<a href="">
														<div><img src="{{$image_url}}{{$sections[11]->section_image}}" alt="" /></div>
														<p>{!! $sections[11]->section_description !!}</p>
													</a>
												</li>
												<li>
													<a href="">
														<div><img src="{{$image_url}}{{$sections[12]->section_image}}" alt="" /></div>
														<p>{!! $sections[12]->section_description !!}</p>
													</a>
												</li>

											</ul>
										</section>
								</div>
							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<footer id="footer" class="container">
						<div class="row">
							<div class="8u 12u(mobile)">

								<!-- Links -->
									<section>
										<h2>Other links</h2>
										<div>
											<div class="row">
												<div class="3u 12u(mobile)" style="width: 72% !important;">
													{!! $sections[10]->section_description !!}
												</div>
												
											</div>
										</div>
									</section>

							</div>
							<div class="4u 12u(mobile)">

								<!-- Blurb -->
									<section>
										<h2>Project Leader:</h2>
										<p>
											Cyril RAY<br/>
											e-mail: <a href="mailto:cyril.ray@ecole-navale.fr">cyril.ray@ecole-navale.fr</a><br/>
											Phone: <a href="tel:+33 2 98 23 36 11">+33 2 98 23 36 11</a></br>
											Address: BCRM Brest
													GIP Ecole Navale IRENav
													CC 600
													29240 Brest Cedex 9
										</p>
									</section>

							</div>
						</div>
					</footer>
				</div>

			<!-- Copyright -->
				<div id="copyright">
					&copy;</a>
				</div>

		</div>

		<!-- Scripts -->
			<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
			<script src="{{ asset('assets/js/skel.min.js') }}"></script>
			<script src="{{ asset('assets/js/skel-viewport.min.js') }}"></script>
			<script src="{{ asset('assets/js/util.js') }}"></script>
			<!--[if lte IE 8]><script src="{{ asset('assets/js/ie/respond.min.js') }}'"></script><![endif]-->
			<script src="{{ asset('assets/js/main.js') }}"></script>
			<script>
			$('a[href^=#]').on("click",function(e){
		var t= $(this.hash);
		var t=t.length&&t||$('[name='+this.hash.slice(1)+']');
		if(t.length){
			var tOffset=t.offset().top;
			$('html,body').animate({scrollTop:tOffset-20},'slow');
			e.preventDefault();
		}
	});
			</script>

	</body>
</html>