<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index(){

        $url = "http://deais_cms.app/uploads/images/";

        $sections = DB::table('Sections')->get();
        $publications = DB::table('Publications')->get();
        $partners = DB::table('Partenaires')->get();
        $references = DB::table('Reference')->get();
        
        return view('index', ['sections' => $sections, 
                              'image_url' => $url,
                              'publications' => $publications,
                              'partners' => $partners,
                              'references' => $references
                              ] );

    }
}